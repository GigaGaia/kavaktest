package com.example.myapplication.ui.recycler_view

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

import com.example.myapplication.R
import com.example.myapplication.data.entities.Brastlewark
import com.example.myapplication.glide.GlideApp
import com.example.myapplication.ui.activities.MainActivity.Companion.DETAIL
import ru.terrakok.cicerone.Router

class MembersAdapter (private val myDataset: List<Brastlewark>?, private val contextLocal: Context,val navigation: Router?) :

    RecyclerView.Adapter<MembersAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val constraintLayout: ConstraintLayout) : RecyclerView.ViewHolder(constraintLayout){

        var textNombre : TextView = constraintLayout.findViewById(R.id.textName)
        var textProfession : TextView = constraintLayout.findViewById(R.id.textProfessions)
        var imageProfile : ImageView = constraintLayout.findViewById(R.id.imageProfilePicture)

    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MembersAdapter.MyViewHolder {
        // create a new view
        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.members_layout, parent, false) as ConstraintLayout
        // set the view's size, margins, paddings and layout parameters

        return MyViewHolder(textView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        var brastlewark:Brastlewark? = myDataset?.get(position)
        brastlewark?.let{
            holder.textNombre.text = it.name
            holder.textProfession.text =it.professions.toString()
            Log.i("Adapter",it.thumbnail)
            GlideApp.with(contextLocal).load(it.thumbnail).override(250,250).into(holder.imageProfile)
        }
        holder.constraintLayout.setOnClickListener() {
                v: View ->
            navigation?.navigateTo(DETAIL,brastlewark?.name)

        }


    }


    override fun getItemCount() = myDataset?.size!!
}