package com.example.myapplication.ui.activities

import android.content.Intent
import android.os.Bundle
import android.preference.EditTextPreference
import android.util.Log
import android.widget.EditText
import android.widget.ImageView

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.App
import com.example.myapplication.R
import com.example.myapplication.data.entities.Brastlewark
import com.example.myapplication.db
import com.example.myapplication.glide.GlideApp
import com.example.myapplication.ui.recycler_view.MemberThumbnailAdapter
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class MemberDetailActivity :AppCompatActivity(){


    lateinit var name:EditText
    lateinit var age:EditText
    lateinit var weight:EditText
    lateinit var height:EditText
    lateinit var hairColor:EditText
    lateinit var image:ImageView
    lateinit var recyclerViewMembersThumbnails:RecyclerView
    lateinit var drastleWarksData: Flowable<List<Brastlewark>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.member_profile)
        name = findViewById(R.id.etName)
        age = findViewById(R.id.etAge)
        weight = findViewById(R.id.etWeight)
        height = findViewById(R.id.etHeight)
        hairColor = findViewById(R.id.etHairColor)
        image = findViewById(R.id.imageView)
        var intentLocal: Intent = intent
        recyclerViewMembersThumbnails = findViewById(R.id.recyclerView)
        drastleWarksData = db.brastlwwarkDao().getBrastlewark(intentLocal.getStringExtra("name"))

        drastleWarksData.observeOn(AndroidSchedulers.mainThread())
            //Subscribe to the Observer away from the main UI thread//
            .subscribeOn(Schedulers.io())
            .subscribeBy(onNext = {lista->
                var  brastleWark:Brastlewark =  lista.get(0)
                name.setText(brastleWark.name)
                age.setText(brastleWark.age.toString())
                weight.setText(brastleWark.weight.toString())
                height.setText(brastleWark.height.toString())
                hairColor.setText(brastleWark.hairColor.toString())
                GlideApp.with(this).load(brastleWark.thumbnail).override(250,250).into(image)
                var friendList:List<String>? = brastleWark.friends
                friendList?.let{
                    var drastleWarksFriends: Flowable<List<Brastlewark>> =  db.brastlwwarkDao().getBrastlewarkList(it)
                    drastleWarksFriends.observeOn(AndroidSchedulers.mainThread())
                        //Subscribe to the Observer away from the main UI thread//
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(onNext = {friendList->
                            recyclerViewMembersThumbnails.adapter = MemberThumbnailAdapter(friendList,this)
                            var linearLayoutManager:LinearLayoutManager = LinearLayoutManager(this)
                            linearLayoutManager.orientation = HORIZONTAL
                            recyclerViewMembersThumbnails.layoutManager =linearLayoutManager

                        },onError={error->
                            Log.i("MembarDetailActivity","error:"+error)
                        })
                }


            },onError =  {error->})








    }
}