package com.example.myapplication.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.App
import com.example.myapplication.R
import com.example.myapplication.db
import com.example.myapplication.di.components.DaggerMainActivityBindings
import com.example.myapplication.di.modules.BrastlewarkRepositoryImplModule
import com.example.myapplication.ui.recycler_view.MembersAdapter
import io.reactivex.disposables.CompositeDisposable

import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    companion object {
        val TAG: String = "LoginRegisterActivity"
        val DETAIL:String = "DETAIL"
    }



    @Inject
    lateinit var bRM: BrastlewarkRepositoryImplModule

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    private val router: Router? by lazy { App.INSTANCE.cicerone.router }

    lateinit var recyclerView:RecyclerView

    private val navigator: Navigator? by lazy {
        object : Navigator {
            override fun applyCommand(command: Command) {   // 2
                if (command is Forward) {
                    forward(command)
                }
            }

            private fun forward(command: Forward) {
                when (command.screenKey) {

                    DETAIL ->{
                        var intentLocal = Intent(this@MainActivity, MemberDetailActivity::class.java)
                        Log.i(TAG,"command.transitionData:"+command.transitionData)
                        intentLocal.putExtra("name",command.transitionData as String)
                        startActivity(intentLocal)
                    }



                    else -> Log.e("Cicerone", "Unknown screen: " + command.screenKey)
                }
            }

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        DaggerMainActivityBindings.create().inject(this)
        recyclerView = findViewById(R.id.recyclerView)
        compositeDisposable.add(bRM.getBrastlewarkRepositoryImplModule().getBrastlewarks(
            onError={error->Log.e("Main",""+error)},
            onNext={ resultado->
                db.brastlwwarkDao().deleteteBrasterwark()
                Log.i("Main","resultado.brastlewark?.size:"+resultado.brastlewark?.size)
                resultado.brastlewark?.let{
                    db.brastlwwarkDao().insertMultiple(it)
                }

                var membersAdapter:MembersAdapter = MembersAdapter(resultado.brastlewark,this,router)
                recyclerView.adapter = membersAdapter
                recyclerView.layoutManager = LinearLayoutManager(this)
            }))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        super.onResume()
        //presenter.onCreate()
        App.INSTANCE.cicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        App.INSTANCE.cicerone.navigatorHolder.removeNavigator()
    }
}
