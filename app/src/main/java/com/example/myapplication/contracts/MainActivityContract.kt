package com.example.myapplication.contracts

import com.example.myapplication.data.entities.Brastlewark
import com.example.myapplication.data.entities.BrastlewarkList
import io.reactivex.disposables.Disposable

interface MainActivityContract {

    interface View{
        fun login()
        fun showError()
        fun register()
        fun passwordRecovery()
    }

    interface Presenter{
        fun onCreate()
        fun onDestroy()
        //fun onLogin(userData: UserData)
        fun onError()
        //fun onRegister(userData: UserData?)
        fun onCancel()
        //fun onPasswordRecovery()
    }

    interface Interactor{
        fun onGetBrastlebark(onError: (Throwable) -> Unit,
                             onNext: (BrastlewarkList) -> Unit): Disposable
        //fun onSubmit(userData:UserData): UserData
//        fun onRegister(userData:UserData):UserData
        //fun onPasswordLost()
    }


    interface  InteractorOutput{
        fun onQuerySuccess():BrastlewarkList
        fun onQueryError()
    }









}