package com.example.myapplication.interactors

import com.example.myapplication.contracts.MainActivityContract
import com.example.myapplication.data.entities.BrastlewarkList
import com.example.myapplication.data.repositories.implementations.BrastlewarkRepositoryImpl
import com.example.myapplication.di.components.DaggerMainActivityBindings
import com.example.myapplication.di.modules.BrastlewarkRepositoryImplModule
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainActivityInteractor:MainActivityContract.Interactor {

    constructor(){
        DaggerMainActivityBindings.create().inject(this)
    }


    @Inject
    lateinit var repository:BrastlewarkRepositoryImpl



    override fun onGetBrastlebark(
        onError: (Throwable) -> Unit,
        onNext: (BrastlewarkList) -> Unit):Disposable {
        return repository.getBrastlewarks(onError,onNext)
    }

}