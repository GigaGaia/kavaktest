package com.example.myapplication

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.example.myapplication.data.db.BrastlewarkDB
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

lateinit var db: BrastlewarkDB

class App : MultiDexApplication(){

    companion object {
        lateinit var INSTANCE: App

    }

    init {
        INSTANCE = this
    }

    lateinit var cicerone: Cicerone<Router>

    override fun onCreate() {
        super.onCreate()
        db = BrastlewarkDB.getInstance(this)
        INSTANCE = this
        this.initCicerone()
    }

    private fun initCicerone() {
        this.cicerone = Cicerone.create()
    }

    public fun  navigate(){

    }





}

