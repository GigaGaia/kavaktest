package com.example.myapplication.data.repositories.implementations

import com.example.myapplication.data.entities.Brastlewark
import com.example.myapplication.data.entities.BrastlewarkList
import com.example.myapplication.data.entities.GenericMessage
import com.example.myapplication.data.net.DoBrastlewark
import com.example.myapplication.data.repositories.interfaces.BrastlewarkRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class BrastlewarkRepositoryImpl @Inject constructor(private val restrofitClient:Retrofit?): BrastlewarkRepository  {
    override fun insert(
        brastlewark: Brastlewark,
        onError: (Throwable) -> Unit,
        onNext: (GenericMessage) -> Unit
    ) {

    }

    override fun getBrastlewarks(
        onError: (Throwable) -> Unit,
        onNext: (BrastlewarkList) -> Unit
    ): Disposable {
        return restrofitClient?.create(DoBrastlewark::class.java)?.getBrastlewark()!!
                        .observeOn(AndroidSchedulers.mainThread())
                //Subscribe to the Observer away from the main UI thread//
                .subscribeOn(Schedulers.io())
                .subscribeBy(onNext = onNext,onError =  onError)

    }

    override fun delete(
        brastlewark: Brastlewark,
        onError: (Throwable) -> Unit,
        onNext: (GenericMessage) -> Unit
    ) {

    }

    override fun update(
        brastlewark: Brastlewark,
        onError: (Throwable) -> Unit,
        onNext: (GenericMessage) -> Unit
    ) {

    }

}