package com.example.myapplication.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.myapplication.data.entities.Brastlewark
import io.reactivex.Flowable

@Dao
interface BrastlewarkDAO {


    @Insert(onConflict = REPLACE)
    fun insert (brastlewark: Brastlewark)

    @Insert(onConflict = REPLACE)
    fun insertMultiple (item:List<Brastlewark>)

    @Query("select * from Brastlewark")
    fun getBrastlewarks():Flowable<List<Brastlewark>>

    @Query("select * from Brastlewark where name=:name")
    fun getBrastlewark(name:String): Flowable<List<Brastlewark>>

    @Query("select * from Brastlewark where name in (:name)")
    fun getBrastlewarkList(name:List<String>): Flowable<List<Brastlewark>>

    @Query("delete from Brastlewark")
    fun deleteteBrasterwark();
}