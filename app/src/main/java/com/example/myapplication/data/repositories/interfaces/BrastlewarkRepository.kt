package com.example.myapplication.data.repositories.interfaces

import com.example.myapplication.data.entities.Brastlewark
import com.example.myapplication.data.entities.BrastlewarkList
import com.example.myapplication.data.entities.GenericMessage
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface BrastlewarkRepository {

    fun insert(brastlewark: Brastlewark, onError: (Throwable) -> Unit ,onNext: (GenericMessage) -> Unit)
    fun getBrastlewarks( onError: (Throwable) -> Unit ,onNext: (BrastlewarkList) -> Unit): Disposable
    //fun getCreations(, onError: (Throwable) -> Unit ,onNext: (Tokens) -> Unit):List<CreationData

    fun delete(brastlewark: Brastlewark, onError: (Throwable) -> Unit ,onNext: (GenericMessage) -> Unit)

    fun update(brastlewark: Brastlewark, onError: (Throwable) -> Unit ,onNext: (GenericMessage) -> Unit)


}