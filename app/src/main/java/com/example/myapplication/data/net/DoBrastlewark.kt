package com.example.myapplication.data.net


import com.example.myapplication.data.entities.BrastlewarkList
import io.reactivex.Observable
import retrofit2.http.GET

interface DoBrastlewark{
    @GET("rrafols/mobile_test/master/data.json")
    fun getBrastlewark(): Observable<BrastlewarkList>
}