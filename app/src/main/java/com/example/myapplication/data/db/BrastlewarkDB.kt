package com.example.myapplication.data.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.myapplication.data.entities.Brastlewark


@Database(entities=[Brastlewark::class],version=1,exportSchema = false)
@TypeConverters(value=[ Converters::class])
abstract class BrastlewarkDB :RoomDatabase(){


    abstract fun brastlwwarkDao():BrastlewarkDAO

    companion object{
        private val lock = Any()
        private const val DB_NAME = "Brastlewark"
        private var INSTANCE: BrastlewarkDB? = null

        fun getInstance(application: Application): BrastlewarkDB {
            synchronized(BrastlewarkDB.lock) {
                if (BrastlewarkDB.INSTANCE == null) {
                    BrastlewarkDB.INSTANCE =
                        Room.databaseBuilder(application, BrastlewarkDB::class.java, BrastlewarkDB.DB_NAME)
                            .allowMainThreadQueries()
                            .build()
                }
            }
            return INSTANCE!!
        }


    }


}