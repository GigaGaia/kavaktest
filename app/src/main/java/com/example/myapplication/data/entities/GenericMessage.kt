package com.example.myapplication.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GenericMessage(   @SerializedName("message")
                             @Expose
                             var message:String? = null)