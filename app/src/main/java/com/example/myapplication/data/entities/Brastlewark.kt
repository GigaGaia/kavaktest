package com.example.myapplication.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@Entity
data class Brastlewark (
    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Int? = null,

    @SerializedName("name")
    @Expose
    var name: String? = null,

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null,

    @SerializedName("age")
    @Expose
    var age: Int? = null,

    @SerializedName("weight")
    @Expose
    var weight: Double? = null,

    @SerializedName("height")
    @Expose
    var height: Double? = null,

    @SerializedName("hair_color")
    @Expose
    var hairColor: String? = null,

    @SerializedName("professions")
    @Expose
    var professions: ArrayList<String>? = null,

    @SerializedName("friends")
    @Expose
    var friends: ArrayList<String>? = null

)
