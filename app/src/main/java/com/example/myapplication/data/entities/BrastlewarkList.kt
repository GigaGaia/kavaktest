package com.example.myapplication.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BrastlewarkList(
    @SerializedName("Brastlewark")
    @Expose
    public var brastlewark: List<Brastlewark>? = null)