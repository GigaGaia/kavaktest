package com.example.myapplication.di.modules

import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@Module
class CompositeDisposableModule @Inject constructor() {
    @Provides
    fun getCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }
}