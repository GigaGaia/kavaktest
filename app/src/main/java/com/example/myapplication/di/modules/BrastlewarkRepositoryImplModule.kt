package com.example.myapplication.di.modules

import com.example.myapplication.data.ServiceGenerator
import com.example.myapplication.data.repositories.implementations.BrastlewarkRepositoryImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Module
class BrastlewarkRepositoryImplModule @Inject constructor(private val retrofit: Retrofit?) {
    @Provides
    @Singleton
    fun getBrastlewarkRepositoryImplModule():BrastlewarkRepositoryImpl{
        return BrastlewarkRepositoryImpl(retrofit)
    }
}