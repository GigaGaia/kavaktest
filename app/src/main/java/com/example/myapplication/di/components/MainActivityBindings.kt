package com.example.myapplication.di.components

import com.example.myapplication.ui.activities.MainActivity
import com.example.myapplication.contracts.MainActivityContract
import com.example.myapplication.di.modules.BrastlewarkRepositoryImplModule
import com.example.myapplication.di.modules.CompositeDisposableModule
import com.example.myapplication.di.modules.RetrofitModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules=[BrastlewarkRepositoryImplModule::class, RetrofitModule::class, CompositeDisposableModule::class])
interface MainActivityBindings {


    //@ContributesAndroidInjector
    fun inject(mainActivity: MainActivity)
    fun inject(mainActivityInteractor: MainActivityContract.Interactor)




}