package com.example.myapplication.di.components

import com.example.myapplication.ui.activities.MainActivity

//@Singleton
//@Component(modules=[BrastlewarkRepositoryImplModule::class,RetrofitModule::class])
interface ApplicationComponent {
    fun inject(mainActivity: MainActivity)
}