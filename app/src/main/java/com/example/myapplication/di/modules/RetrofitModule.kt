package com.example.myapplication.di.modules

import com.example.myapplication.data.ServiceGenerator
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton
@Module
class RetrofitModule @Inject constructor() {
    @Provides
    @Singleton
    fun providesRetrofitClient(): Retrofit?{
        return ServiceGenerator.getClient()
    }
}