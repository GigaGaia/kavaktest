package com.example.myapplication.presenters

import com.example.myapplication.contracts.MainActivityContract
import com.example.myapplication.data.entities.BrastlewarkList
import ru.terrakok.cicerone.Router

class MainActivityPresenter (var view:MainActivityContract.View,var interactor:MainActivityContract.Interactor,var router: Router?):MainActivityContract.Presenter,MainActivityContract.InteractorOutput{


    override fun onCreate() {

    }

    override fun onDestroy() {

    }

    override fun onError() {

    }

    override fun onCancel() {

    }


    override fun onQuerySuccess(): BrastlewarkList {
        return BrastlewarkList()
    }

    override fun onQueryError() {

    }

}